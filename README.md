# QG-MHD_modon

Python codes (using the library Dedalus) and jupyter notebook used for processing and analysing numerical simulations of the single layer Magneto-Quasi-Geostrophic equations, initialized with a magnetic modon (dipolar vortex solution). 
