""" tools for dedalus """

import numpy as np
from tools_grid import azim_avg

def get_spectrum(field, var_pres=True, return_rad=True):
    """ return azimuthal average of the PSD of a field """
    kx, ky = field.domain.all_elements()
    modk = (kx**2 + ky**2)**.5
    psd, modk = azim_avg(abs(field['c'])**2, rad=modk, dr=kx[1]-kx[0], var_pres=var_pres, return_rad=return_rad)
    return psd, modk

def get_barycenter(xx, weight, axis=None):
    """ ascending order is assumed
    axis=None will compute the total barycenter.
    Specify axes will compute vector (table) of barycenters """
    xi = xx.ravel()[0]
    xx -= xi
    xe = xx.max()
    theta = xx/xe * 2 * np.pi
    Mtot = weight.sum(axis=axis)
    cosm = (weight * np.cos(theta)).sum(axis=axis) / Mtot
    sinm = (weight * np.sin(theta)).sum(axis=axis) / Mtot
    thetam = np.arctan2(-sinm, -cosm) + np.pi
    return xe*thetam/2/np.pi + xi

def length_per(xnew, xold, Lper):
    """ compute position difference in a periodic domain, taking the minimal result"""
    dx = xnew - xold
    res = np.array([dx % Lper, (-dx) % Lper])
    i = abs(res).argmin()
    return (-1)**i * res[i]
