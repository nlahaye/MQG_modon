#!/usr/bin/env python
# coding: utf-8

# # Forced TQG turbulence
# Non-dimensionalized QG MHD equations (cf. eg. [Zeitlin 2013]): 
# $$\partial_t q + J(\psi,q) = J(b,\Delta b)\; (+ D - A + F), \\ 
# \partial_t b + J(\psi,b) = 0 \; (+D_b - A_b(b+\psi)) $$ 
# with $q=\Delta\psi-\psi/Bu \;(+\beta y)$.
# $b$ is the magnetic potential ($A$ in the ref., named "buoy" throughout this notebook), $\Psi$ the streamfunction. 
# 
# * Viscosity: operator of the form $D=-\nu(-\Delta)^\alpha(\Delta\psi)$ ([Farge & Sadourny 1989]: $\nu = \tau_Z^{-1}k_\text{max}^{-2\alpha}$ with $\tau_Z\propto Z^{-1/2}$)
# * Damping (only momentum so far): Rayleigh $A = r \Delta\psi$ or hypo: $A = -r \psi$. General form: $A = (-1)^q r_q \Delta^{-2(q-1)}\psi$. $q=0$ or $1$ for now.
# * Diffusion for buoyancy (and passive tracer field) of the same form as for the momentum. I should check that it is correct for the buoyancy. No damping so far
# * Energy dissipation is: $\frac{dE}{dt} = \frac{1}{2}\frac{d}{dt}\int |k^2||\psi^2| + |\psi|^2/Bu = -\nu \int \Delta\psi\Delta^{\alpha}\psi$ (and $\zeta=\Delta\psi$ the relative vorticity)
# 
# adapted from TQG_turbu_forced_de_w-diag.ipynb, started on 2021/01/14
# 
# ### Ongoing
# * writing analysis instructions
# * TODOs: write json file (or attributes?) with parameters; add merging instructions at the end (only over distributed files, not time); find how to store forcing

#import matplotlib as mpl
#import matplotlib.pyplot as plt
#mpl.use("Agg")

import numpy as np
#import scipy.interpolate as itp
import scipy.integrate as itg
from mpi4py import MPI
CW = MPI.COMM_WORLD
#import h5py
from dedalus import public as de
from dedalus.extras import flow_tools
#from dedalus.extras import plot_tools
import time, sys, os
import scipy.signal as sig
from xarray import open_dataset

import tools_deda as tod
#import tools_grid as tog
from modon_QGMHD import modon
#import QGMHD_jets as jet

import logging
root = logging.root   
logger = logging.getLogger(__name__)

### Global parameters:
Ny = 1024
Nx = (Ny * 11)//8
sim_end_time = 80.#
delt_show = .5  # log info every...
delt_snap = .5  # store snapshots every...
delt_diag = .1  # store int. diags every...
Bu = 1 # (L_flow/Rd)**2 and Rd = 1; Bu=inf -> 2D incompressible
beta_f = False # f-plane: 0 (or False); beta-plane: something. 
dt_init = 1e-3  # would be better to choose this from IC (CFL)
do_plot = True  # call plotting function at the end (will merge)

# Initial conditions (spectral bump with mean KE = 1; or 0)
add_noiz = False # small scale noise. set False or None for NO, scalar for yes (amplitude)
k_0 = 256./4./np.pi # inverse wavelength adim by Lx;  # near spectral peak
coefs_ke = 8, 22
smooth_init = 2.*(Nx/512) # False for no smoothing, else float such that k_tronc = kmax/smooth_init

### Tracers: passive and active (magnetic potential)...
Pr_b = 1. # (hyper)diffusivity/(hyper)viscosity
if len(sys.argv)>1:
    buoy_amp = float(sys.argv[1]) #, meaning depends on IC type"
else:
    buoy_amp = .0 # "see below, meaning depends on IC type"
if len(sys.argv)>2:
    init_buoy = float(sys.argv[2])
else:
    init_buoy = 0. # I should have "random", "iso_psi", "iso_PV", "iso-u"" # float for modon
do_trac = False   # add passive tracer advection
Pr_t = Pr_b # (hyper)diffusivity/(hyper)viscosity
init_trac = "iso_b" # "iso_PV": s \propto Q; "cos_bump"; "iso_b" (if do_tqg); "iso_p"
trac_amp = 1 # see IC, meaning depends on type

# (hyper) viscosity
do_visc = 1 # for molecular viscosity, int for hyper viscosity, False for no viscosity
Kv_coef = 5e-4 # viscosity (modon) or coef in front of $1/(\tau_z k_\text{max}^(2\alpha)$ (random IC)

# damping (large scale -- Rayleigh or first order hypo for now)
do_damp = True # True for moving sponge layer
Amo = 0.5 # damping rate
Ltr = .2 # transition width of sponge layer

rnd_seed = CW.Get_rank()  # random generator seed (use None for "true random", int for reproductibility)

############################################################
#####  ---  End of user-defined parameter section ---  #####
############################################################

# prepare path for storing things
kap_txt = "{:.2f}".format(buoy_amp).replace(".","p").replace("-","m")
if not kap_txt.startswith("m"): kap_txt = "p"+kap_txt
kae_txt = "{:.2f}".format(init_buoy).replace(".","p").replace("-","m")
if not kae_txt.startswith("m"): kae_txt = "p"+kae_txt
nu_txt = int(abs(np.log10(Kv_coef)*10))
nu_txt = f"{int(do_visc)}nu{nu_txt}" if do_visc else "novisc"
Pr_txt = "{:.1f}".format(Pr_b).replace(".","p")
nx_str = "{:03d}".format(Ny) if Ny//Nx==1 else "{:03d}d".format(Ny)
base_name = "modon"
if add_noiz:
    base_name += "p"

store_dir = os.getenv("SCRATCH")+"/MHD/data/{0}_N{1}_Bu{2}_kai{3}_kax{4}_{5}_Pr{6}".format(
        base_name, nx_str, "{:.1f}".format(Bu).replace(".","p"), kap_txt, kae_txt, nu_txt, Pr_txt)
save_dir = store_dir.replace("data", "runs")

if CW.Get_rank() == 0:
    if not os.path.isdir(store_dir): os.mkdir(store_dir)
    if not os.path.isdir(save_dir): os.mkdir(save_dir)

Ly = 5./Bu**.5
Lm = -1.
Lx = Ly * Nx/Ny
logging.info(f"Ly is {Ly}, Lx is {Lx}")
k_min, k_max = np.pi/Ly/(1-Lm), Ny*np.pi/2./Ly/(1-Lm)
k_0 *= np.pi / Ly
    
### Create bases and domain
x_b = de.Fourier('x', Nx, interval=(Lm*Lx, Lx), dealias=3/2)
y_b = de.Fourier('y', Ny, interval=(Lm*Ly, Ly), dealias=3/2)
domain = de.Domain([x_b, y_b], grid_dtype=np.float64)

xx, yy = domain.all_grids()
kx, ky = domain.all_elements()
x_dea, y_dea = domain.all_grids(scales=domain.dealias)
modk = ( kx**2 + ky**2 )**.5
dxs, dys = domain.all_grid_spacings()
dxd, dyd = domain.all_grid_spacings(scales=domain.dealias)

# ## Initial conditions
# * modon dipolar solution

### Initial conditions
psi = domain.new_field(name='psi')
buoy = domain.new_field(name='b')

if not isinstance(init_buoy, float):
    init_buoy = 0.
file_init_guess = "alphas_vs_kap_kam.nc"
if os.path.exists(file_init_guess):
    ds = open_dataset(file_init_guess)
    in_guess = float(ds["alpha"].sel(kap_ext=abs(init_buoy), kap_int=buoy_amp, method="nearest"))
else:
    in_guess=None
res = modon(xx, yy, kappa=buoy_amp, kapap=init_buoy, a=1./Bu**.5, init_guess=in_guess)
psi["g"] = res[0]
tapper = sig.tukey(domain.global_grid_shape(1)[1], alpha=.1)
slices = domain.dist.grid_layout.slices(scales=1)
psi["g"] *= tapper[None,slices[1]]
buoy["g"] = res[1]

if smooth_init: # smoothing
    logging.info("smoothing with ktronc={:.1e}".format(k_max/float(smooth_init)))
    kern = np.exp(-.5*(modk*float(smooth_init)/k_max)**2)
    buoy["c"] *= kern
    psi["c"] *= kern

if add_noiz:
    # seed random number generator
    rng = np.random.default_rng(seed=rnd_seed)
    def rand_angle(size=None):
        """ wrapper for np.random.default_rng.random """
        return rng.uniform(low=0, high=2*np.pi, size=size)

    # spectral bump as IC: normalize such than mean KE is 1
    def amp(modk, k0, coefs=coefs_ke):
        a, b = coefs
        fun = lambda k: k**a/(k+k0)**b
        norm, _ = itg.quad(lambda k: k*fun(k), k_min, k_max)
        # normalize such that mean KE is 1: sum psi**2 = int(psi**2) * dk**2 = int(amp)
        return fun(modk)/(norm*np.pi/kx[1]**2)

    ### add noise to initial conditions
    noiz = domain.new_field(name='noiz')
    noiz['c'] = np.where(modk>0, amp(modk, k_0)**.5/modk * np.exp(1.j*rand_angle(noiz['c'].shape)), 0)
    psi['g'] += psi["g"] * add_noiz * noiz["g"]
    noiz['c'] = np.where(modk>0, amp(modk, k_0)**.5 * np.exp(1.j*rand_angle(buoy['c'].shape)), 0)
    buoy['g'] += psi["g"] * add_noiz * noiz["g"]
    
q_a = psi.differentiate(x=2) + psi.differentiate(y=2) - psi/Bu  # PV anomaly (no planetary)

if do_trac: ### build initial tracer field
    s_t = domain.new_field(name="s")
    if init_trac == "cos_bump":
        radadim = ((xx/Lx)**2+(yy/Ly)**2)**.5
        s_t['g'] = np.where(radadim<1, (1+np.cos(radadim*np.pi)), 0.)
    elif init_trac == "iso_PV":
        s_t['c'] = trac_amp * q_a['c']
    elif init_trac == "iso_b":
        s_t["g"] = buoy["g"]
    else:
        raise ValueError('unrecognised option for passive tracer field initialization')
    s_t['g'] /= (s_t['g']**2).mean()**.5 # mean variance is 1

### Info
ke_dens = abs(modk*psi['c'])**2
ke_moy = (ke_dens.sum() + ke_dens[1:].sum())/2. # mean kinetic energy
za_dens = abs(q_a["c"])**2
za_moy = (za_dens.sum() + za_dens[1:].sum())/2. # mean standard enstrophy
logger.info("mean KE is {:.2f}".format(ke_moy))

# compute viscosity coefficient based on timescale
if do_visc:
    a_visc = int(do_visc)
    Kv = Kv_coef
    logger.info("viscosity coefficient is {0:.2e} [L^{1:d}/T]".format(Kv,2*a_visc))
    
my_vars = ['psi', 'b']
if do_trac:
    my_vars.append("s")
    
problem = de.IVP(domain, variables=my_vars)
problem.substitutions['Lap(A)'] = "dx(dx(A)) + dy(dy(A))"
problem.substitutions['Jac(A,B)'] = "dx(A)*dy(B) - dy(A)*dx(B)"
problem.substitutions['Adv(A)'] = "Jac(psi,A)" #"dx(psi)*dy(A) - dy(psi)*dx(A)"
if beta_f:
    problem.parameters['beta_f'] = beta_f
problem.parameters['Bu'] = Bu
problem.parameters['B_b'] = init_buoy 
problem.substitutions['q'] = 'Lap(psi) - psi/Bu'

if do_visc: ### viscosity
    problem.parameters['Kv'] = Kv
    sgn = '(-1) * ' if (a_visc%2)==0 else ''
    visc = sgn+'(Lap'*a_visc+'(A)'+')'*a_visc
    logger.info("putting (hyper) viscosity of the form: {}".format(visc))
    problem.substitutions['visc(A)'] = visc
else:
    problem.substitutions['visc(A)'] = '0'
    problem.parameters['Kv'] = 0
if do_damp:
    logger.info("putting moving sponge layer")
    problem.parameters['Amo'] = Amo
    problem.parameters["c"] = 1.
    problem.parameters["Ldom"] = 2*Lx
    problem.parameters["Ltra"] = Ltr
    problem.parameters["Lwin"] = 2*Ly
    problem.parameters["x0"] = 0
    problem.parameters["tin"] = 0
    problem.substitutions["xon"] = "x0 + c*(t-tin)"
    problem.substitutions["amo(t,x)"] = "1 - ( tanh((x-(xon-Lwin/2))/Ltra) - tanh((x-(xon+Lwin/2))/Ltra)" +\
                                        " + tanh((x-(xon-Ldom-Lwin/2))/Ltra) - tanh((x-(xon-Ldom+Lwin/2))/Ltra) " +\
                                        " + tanh((x-(xon+Ldom-Lwin/2))/Ltra) - tanh((x-(xon+Ldom+Lwin/2))/Ltra) )/2"

problem.parameters['Pr'] = Pr_b
if do_trac:
    problem.parameters['Pr_t'] = Pr_t

### construct equation
eq_LHS = "dt(q) - Kv*Lap(visc(psi)) + B_b*dx(Lap(b))"    
eq_RHS = "-Adv(q) + Jac(b, Lap(b))" #"dy(psi)*dx(q) - dx(psi)*dy(q)"
if beta_f:
    eq_LHS += " + beta_f * dx(psi)"
if do_damp:
    eq_RHS += " - Amo*amo(t,x)*q"
if np.isinf(Bu): # need a gauge condition
    problem.add_equation(eq_LHS+" = "+eq_RHS, condition="(nx != 0) or (ny != 0)")
    problem.add_equation("psi = 0", condition="(nx == 0 ) and (ny == 0)")
else:
    problem.add_equation(eq_LHS+" = "+eq_RHS)
eq_LHS = "dt(b) - Kv*Pr*visc(b) + B_b*dx(psi)"
eq_RHS = "-Adv(b)"
if do_damp:
    eq_RHS += " - Amo*amo(t,x)*b"
problem.add_equation(eq_LHS+" = "+eq_RHS, condition="(nx != 0) or (ny != 0)") #dy(psi)*dx(b) - dx(psi)*dy(b)")
problem.add_equation("b = 0", condition="(nx == 0 ) and (ny == 0)")
if do_trac:
    problem.add_equation("dt(s) - Kv*Pr_t*visc(s) = -Adv(s)") #dy(psi)*dx(s) - dx(psi)*dy(s)")
logger.info("solving the following equations:")
for item in problem.eqs:
    logger.info("\t {0},\t (cond: {1})".format(item['raw_equation'],item['raw_condition']))

solver = problem.build_solver(de.timesteppers.RK443) # SBDF2; RK443
# Initialize
solver.state['psi']['c'] = psi['c'] #0. 
solver.state['b']['c'] = buoy['c']
if do_trac:
    solver.state['s']['g'] = s_t['g'] #0. #
    
q_a_i = q_a.evaluate()['g']
b_i = buoy['g']
    
dt = dt_init
solver.stop_sim_time = sim_end_time
solver.max_step = np.inf
cfl = flow_tools.CFL(solver, dt, safety=0.5, max_change=1.5, min_change=0.5, threshold=0.05)
cfl.add_velocities(('dy(psi)','dx(psi)'))

### setup diags
path_2D = os.path.join(store_dir, "snapshots")
path_int = os.path.join(store_dir, "integrals")

ana_2D = solver.evaluator.add_file_handler(path_2D, sim_dt=delt_snap, max_writes=50, mode="overwrite")
ana_int = solver.evaluator.add_file_handler(path_int, sim_dt=delt_diag, max_writes=np.inf, mode="overwrite")

# add tasks
ana_2D.add_system(solver.state, layout="g")
ana_2D.add_task("dx(dx(psi)) + dy(dy(psi))", layout="g", name="vort")

ana_int.add_task("integ(.5*psi**2/Bu, 'x', 'y')", name="<Ep>")
ana_int.add_task("integ(.5*(dx(psi)**2 + dy(psi)**2), 'x','y')", name='<Ek>')
ana_int.add_task("integ(.5*(dx(b)**2 + dy(b)**2), 'x','y')", name='<Em>')
ana_int.add_task("integ(Kv*Lap(psi)*visc(psi), 'x', 'y')", name = "<Dv>")
if do_damp:
    ana_int.add_task("integ(Amo*psi*amo(t,x)*q, 'x', 'y')", name="<Da>")

### Loop over time 
psi = solver.state['psi']
psi.set_scales(domain.dealias)
q_a = psi.differentiate(x=2) + psi.differentiate(y=2) - psi/Bu  # PV anomaly (no planetary, nor thermal)
buoy = solver.state['b']
buoy.set_scales(domain.dealias)
if do_trac:
    s_t = solver.state['s']
    s_t.set_scales(domain.dealias)
xm, ym = np.meshgrid(x_dea,y_dea)

### GO FOR IT: main loop
logger.info('Starting loop')
start_time = time.time()
tsim = solver.sim_time
wall_time = time.time()
it_count = 0
if do_damp:
    tupa = solver.sim_time
    xnew = problem.parameters["x0"]
while solver.ok:
    dt = cfl.compute_dt()
    solver.step(dt)
    if do_damp and solver.sim_time > tupa + delt_diag:
        ke = (psi.differentiate(x=1)**2 + psi.differentiate(y=1)**2).evaluate()
        ke.set_scales(1)
        xold, xnew = xnew, tod.get_barycenter(xx.copy(), ke["g"], axis=None)
        speed = tod.length_per(xnew, xold, 2*Lx)/(solver.sim_time-tupa)
        solver.problem.namespace["c"].value = speed
        solver.problem.namespace["x0"].value = xnew
        solver.problem.namespace["tin"].value = solver.sim_time
        logger.info(f"xold: {xold:.2f}, xnew: {xnew:.2f}, speed: {speed:.2f}")
        tupa = solver.sim_time
    if solver.sim_time > tsim + delt_show:
        tsim += delt_show
        if CW.Get_rank() == 0:
            wall_time = (time.time() - wall_time)/(solver.iteration - it_count)
        logger.info('\n\t Iteration: %i, Time: %e, dt: %e, wall_time/it: %e ms' %(
            solver.iteration, solver.sim_time, dt, wall_time*1e3))
        if CW.Get_rank() == 0:
            wall_time, it_count = time.time(), solver.iteration

if do_plot:
    import pathlib
    from dedalus.tools import post
    from dedalus.tools.parallel import Sync
    from plot_2D_rsw import main as main_plot

    with Sync() as sync:
        if sync.comm.rank == 0:
            print("\n now merging over processes")
    post.merge_process_files(path_2D, cleanup=True)
    path = pathlib.Path(path_2D)
    set_paths = list(path.glob(path.name+"_s*.h5"))
    with Sync() as sync:
        if sync.comm.rank == 0:
            print("\n now merging over sets")
    post.merge_sets(path.joinpath(path.name+".h5"), set_paths, cleanup=True)
    output_path = pathlib.Path(store_dir.replace("data","figures")).absolute()
    data_files = str(path/"snapshots.h5")
    with Sync() as sync:
        if sync.comm.rank == 0:
            print("output directory:",output_path)
            if not output_path.exists():
                output_path.mkdir()
            print(data_files)
    post.visit_writes([data_files], main_plot, output=output_path)

