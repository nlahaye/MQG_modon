import numpy as np
import sympy as sy
from scipy.optimize import fsolve

def modon(xx, yy, kappa=0, kapap=0., a=1, init_guess=None):
    """ WARNING: if kapap != 0, returns buoyancy anomaly """
    
    sy_r = sy.symbols("r")
    sy_al, sy_kap, sy_kam = sy.symbols("al, kap, kam")
    sy_a = sy.symbols("a")
    lam = sy.sqrt(-(1+sy_al)/(1-sy_kam**2))

    k1 = sy.besselk(1, sy_r/sy.sqrt(1-sy_kap**2))
    j1 = sy.besselj(1, lam*sy_r)
    Ap = -sy_a/sy.besselk(1, sy_a/sy.sqrt(1-sy_kap**2))
    Am = -sy_a/(1+sy_al)/sy.besselj(1, sy_a*lam)

    if True:
        expr = k1.diff(sy_r).subs(sy_r, sy_a)/Am - j1.diff(sy_r).subs(sy_r,sy_a)/Ap + sy_al/(1+sy_al)/(Am*Ap)
    else:
        expr = Ap*k1.diff(sy_r).subs(sy_r, sy_a) - Am*j1.diff(sy_r).subs(sy_r,sy_a) + sy_al/(1+sy_al)

    params = {"kam":kappa, "kap":kapap, "a":a}
    expr_eval = expr.subs({sy_kam:params["kam"], sy_kap:params["kap"], sy_a:params["a"]})

    
    ### solving
    fun = sy.lambdify(sy_al, expr_eval.subs(sy_al,-sy_al))

    if init_guess is None:
        x0 = 16.3868925833852 # for kappa=0, a=1
    else:
        x0 = init_guess
    res = fsolve(fun, x0=x0/a**2) # scaling the initial guess as a function of a is à la louche
    alpha = -res[0]
    
    if xx.ndim == yy.ndim == 1:
        xx, yy = np.meshgrid(xx, yy)
    elif not (xx.ndim == yy.ndim == 2):
        raise ValueError("inconsitent shapes for x and y arrays")
    rad = (xx**2+yy**2)**.5
    theta = np.arctan2(yy, xx)
    sint = yy/rad

    psi_ou = (Ap * k1).subs({sy_a:params["a"], sy_kap:params["kap"]}) 
    psi_in = (Am * j1 - sy_al/(1+sy_al)*sy_r).subs({sy_a:params["a"], 
                                                    sy_al:alpha, 
                                                    sy_kam:params["kam"]}
                                                  )

    f_psi = sy.lambdify(sy_r, psi_in)
    f_pso = sy.lambdify(sy_r, psi_ou)
    streamf = np.where( rad>params["a"], f_pso(rad), f_psi(rad) ) * sint
    magpot = np.where( rad>params["a"], (f_pso(rad)+rad)*params["kap"], 
                      (f_psi(rad)+rad)*params["kam"]
                     ) * sint
    if kapap != 0:
        magpot -= yy*kapap

    if (rad==0).any():
        streamf[rad==0] = 0
        magpot[rad==0] = 0
        
    return streamf, magpot
