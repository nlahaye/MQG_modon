#!/usr/bin/env python
# coding: utf-8

from matplotlib import pyplot as plt
plt.rcParams.update({"font.family":"serif", "font.size":12})

from pathlib import Path
import os, sys

import numpy as np
import xarray as xr
import dask.array as da
import h5py


def open_dedalus(path, sim_t_as_coord=True, chunks=None):
    nc = h5py.File(path, "r")

    ds = xr.Dataset(data_vars={key:(tuple([dim.label for dim in val.dims]), 
                                    da.from_array(val, chunks=val.chunks)
                                   ) for key,val in nc["tasks"].items()}
                   )
    if chunks is not None:
        ds = ds.chunk(chunks)

    if "t" in ds.dims:
        ds = ds.assign_coords(iteration=xr.DataArray(nc["scales"]["iteration"][:], dims=('t',)), 
                             sim_time=xr.DataArray(nc["scales"]["sim_time"][:], dims=('t',)))
        if sim_t_as_coord:
            ds = ds.rename({"sim_time":"t"})
        else:
            ds = ds.assign_coords(t=xr.DataArray(nc["scales"]["write_number"][:], dims=('t',)))

    space_dims = ["x", "y"]
    for dim in space_dims:
        if dim in ds.dims:
            ds = ds.assign_coords({dim:xr.DataArray(nc["scales"][dim]["1.0"][:], dims=(dim,))})
            ds.attrs["d"+dim] = ds[dim].diff(dim).mean().values
    return ds

def kap_fmt(kap):
    kap_txt = "{:.2f}".format(kap).replace(".","p").replace("-","m")
    if not kap_txt.startswith("m"): kap_txt = "p"+kap_txt
    return kap_txt

data_path = Path(os.getenv("HOME")+"/working_on/MHD/data_work")
kapint, kapext = [float(k) for k in sys.argv[1:3]]
simul = Path("modonp_N1024_Bu1p0_kai{}_kax{}_nu16".format(kap_fmt(kapint),kap_fmt(kapext)))
filetype = "snapshots/snapshots.h5"

ds = open_dedalus(data_path/simul/filetype, sim_t_as_coord=True, chunks={"x":-1, "y":-1})

# ### second type of plots
fies = ["psi", "vort", "B"]
cmaps = {"psi": "PRGn", "B":"BrBG", "vort":"RdBu_r"}
levs_psi = np.linspace(-1.5, 1.5, 10) # even number of levels for 0 in white
tits = [r"$\Psi$", r"$\zeta$", r"$A$"]

slih = slice(-4., 4.)
amp_vort = 20
amp_B = kapext
amp_B_cmap = max(abs(kapext), abs(kapint), .01)

fig_path = Path("./figures")/simul
if not fig_path.exists():
    fig_path.mkdir()

tplots = np.arange(0, 81, 10).tolist()
cbar_kwargs = {"orientation":"horizontal", "label":None}

hpc = {}
for it,tt in enumerate(tplots):
    fig, axs = plt.subplots(1, 3, sharex=True, sharey=True, figsize=(10, 4.5))
    ax = axs[0] # streamfunction
    sds = ds.sel(t=tt, method="nearest").sel(x=slih, y=slih)
    xplot, yplot = sds.x.values, sds.y.values
    #psi = sds.psi.transpose("y", "x").values
    #hpc["psi"] = axs[0].contourf(xplot, yplot, psi, cmap=cmaps["psi"], 
    #                                levels=levs_psi, extend="both")
    #plt.colorbar(hpc["psi"], ax=ax, label="psi", **cbar_kwargs)
    hpp = sds.psi.plot.contourf(ax=ax, y="y", levels=levs_psi, cmap=cmaps["psi"], 
                          extend="both", cbar_kwargs=cbar_kwargs)
    hpp.colorbar.set_ticks([levs_psi[0], 0., levs_psi[-1]])
    ax = axs[1] # vorticity
    hpc["vort"] = sds.vort.plot(ax=ax, cmap=cmaps["vort"], vmin=-amp_vort, vmax=amp_vort, y="y", 
                                cbar_kwargs=cbar_kwargs)
    ax.set_title("")
    ax = axs[2]
    hpc["B"] = (sds.b+amp_B*sds.y).plot(ax=ax, cmap=cmaps["B"], y="y", vmin=-amp_B_cmap, vmax=amp_B_cmap, 
                          cbar_kwargs=cbar_kwargs)
    ax.set_title("")
  
    for ia,ax in enumerate(axs):
        ax.set_xlabel(r'$x$')
        ax.set_ylabel(r'')
        ax.set_aspect(1)
        ax.set_title(tits[ia])

    axs[0].set_ylabel(r'$y$')
    axs[0].text(-.2, 1.02, r"$t=$"+"{:.0f}".format(tt), fontsize=14, transform=axs[0].transAxes)
    
    fig_name = simul.name+"_snapshot_t{:02d}.png".format(round(tt))
    fig.savefig(fig_path/fig_name, dpi=250, bbox_inches="tight")



